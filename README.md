# The service is built on the top of ExpressJS and Sequelize is used as ORM. While PostgreSQL is used as prod database, SQLite is preferred as test database.
# Node target version is 14. 
# Document
-	Http-proxy
https://www.npmjs.com/package/http-proxy
-	Express-gateway
https://www.express-gateway.io/getting-started/

#----------Online-------------
#  cd /online
cd đến mỗi service và thực hiện lần lượt command
# npm install
# npm start

## Khuyến khích sử dụng docker để run nhanh hơn
## Sử dụng giao thức HTTP để gửi vào tạo dữ liệu động cho API gateway này, vui lòng đọc tài liệu. 
architect:
    - gateway: http://localhost:3000/
    - product: http://localhost:3000/product/
    - policy: http://localhost:3000/policy/
 
-------------Use-------------
# online
    #Case 1:Get product (same floor)
    http://localhost:3000/product/

    #Case 2:Get policy (same floor)
    http://localhost:3000/policy/

    #Case 3: Get product from policy (same floor)
    http://localhost:3000/policy/getproduct

# local 
    case 4. getway(local) => getway => product
    http://localhost:4000/product/
    case 5 policy => getway=> product
    http://localhost:4000/policy/getproduct



