idUser = cf7f3bfb-89b1-4d46-bbe8-4994619a6ddb
apiKey = 7vHstbT27ui4tS3LSh0ntg:0lpnwQpPRdrCD18pmBsGfz

# install express-gateway global
npm install -g express-gateway

# generate gateway
eg gateway create

# do same tutorial
https://www.express-gateway.io/getting-started/

# start gateway
cd ./my-gateway
npm start

# Secure the API with Key Authorization
Following steps:
#step1 : create user
$ eg users create
$step 2: Assign the key credential to 'user'
$ eg credentials create -c cf7f3bfb-89b1-4d46-bbe8-4994619a6ddb -t key-auth -q
-------------Access with Key Authorization--------------------
Curl API endpoint as Bob with key credentials 
$curl -H "Authorization: apiKey ${keyId}:${keySecret}" http://localhost:3000/product/

# package or dockerize 
https://hub.docker.com/_/express-gateway

# dynamic config using ADMIN api
https://www.express-gateway.io/docs/admin/

## Sử dụng giao thức HTTP để gửi vào tạo dữ liệu động cho API gateway này, vui lòng đọc tài liệu. 

Proxy:
    - gateway: http://localhost:3000/
    - product: http://localhost:3000/product/
    - policy: http://localhost:3000/policy/
 
-------------Diagram 1-------------
# online
    Case 1.policy => product (same floor)
    http://localhost:3002/policy/getproduct
    Case 2.gateway => product (same floor)
    http://localhost:3000/product/

# local
    Case 3. getway(local) => product
    http://localhost:4000/productpublic/
    Case 4. policy(local) => product
    http://localhost:3003/getproduct/

-------------Diagram 2------------------
# local 
    case 5. getway(local) => getway(server get product)
    http://localhost:4000/product/
    case 6 policy => getway(server get product)
    http://localhost:4000/policy/getproduct