import express from 'express';
import { createProxyMiddleware } from 'http-proxy-middleware';

require('dotenv').config()

const app = express();

app.use('/product',
    createProxyMiddleware(
        {
            target: 'http://localhost:3001', changeOrigin: true,
            pathRewrite: {
                "^/product": "/product/", // rewrite path
            },
        }
    )
)
app.use('/policy',
    createProxyMiddleware(
        {
            target: 'http://localhost:3002', changeOrigin: true,
            pathRewrite: {
                "^/policy": "/policy/", // rewrite path
            },
        }
    )
)
export default app;
