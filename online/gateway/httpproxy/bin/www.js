import httpProxy from 'http-proxy';
// import logger from 'morgan';
import http from 'http';
import app from '../app';

const PORT = process.env.PORT;
app.set('port', PORT);

//----------------------App----------------------------------------
var server = http.createServer(app);
server.listen(PORT);
server.on('listening', onListening);
server.on('error', onError);
function onListening() {
    var addr = server.address();
    console.log("Gateway listening on port:", addr.port);
}
function onError() {
    console.log("Gateway Error");
}



