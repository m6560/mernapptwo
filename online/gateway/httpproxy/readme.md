# init project with express
express --no-view
npm install

npm i http-proxy-middleware
npm i dotenv

npm i --save jsonwebtoken

# write custom auth middleware
https://expressjs.com/en/guide/writing-middleware.html


# run env
Product >> http://localhost:3001/product 
Policy >> http://localhost:3002/policy

Proxy:
 - product: http://localhost:3000/product 
 - policy: http://localhost:3000/policy 