import express from 'express';
import policy from './route/policy.js'
import bodyParser from 'body-parser';
import cors from 'cors';

const app = express();
const PORT = process.env.PORT || 3002

app.use(bodyParser.json({ limit: '30mb' }));
app.use(bodyParser.urlencoded({ extends: true, limit: '30mb' }));
app.use(cors());


app.use('/policy', policy)

app.listen(PORT, () => {
    console.log('policy_online running on port: ' + PORT);
})
