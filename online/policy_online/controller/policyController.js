import ProductService from "../service/productService.js";

export const getPolicy = (req, res) => {
    console.log('connect to policy: success ');
    res.status(200).json({ id: 1, name: 'Gold' })
}
export const createPolicy = (req, res) => {
    console.log('created policy');
    res.status(201).json({ id: 1, name: 'Ben' })
}
export const updatePolicy = (req, res) => {
    console.log('updated policy');
    res.status(204).json({ id: 1, name: 'Ken' })
}
export const getProductData = async (req, res) => {
    try {
        console.log('get dataProduct');
        const data = await ProductService.getProduct();
        console.log('done', data);
        res.status(200).json(data)
    } catch (error) {
        res.status(500).json({ message: error })
    }

}