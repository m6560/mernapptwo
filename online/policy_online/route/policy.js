import express from 'express';
import { getPolicy, createPolicy, updatePolicy, getProductData } from '../controller/PolicyController.js';

const router = express.Router();

router.get('/', getPolicy);
router.get('/getproduct', getProductData);
router.post('/create', createPolicy);
router.post('/update', updatePolicy);

export default router;