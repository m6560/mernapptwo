import express from 'express';
import product from './route/product.js'
import bodyParser from 'body-parser';
import cors from 'cors';

require('dotenv').config()

const app = express();

app.use(bodyParser.json({ limit: '30mb' }));
app.use(bodyParser.urlencoded({ extends: true, limit: '30mb' }));
app.use(cors());

app.use('/product', product)
app.use('/productpublic', product)

export default app;
