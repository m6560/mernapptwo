import express from 'express';
import { getProduct, createProduct, updateProduct } from '../controller/productController.js';

const router = express.Router();

router.get('/', getProduct);
router.post('/create', createProduct);
router.post('/update', updateProduct);

export default router;