import httpProxy from 'http-proxy';
import bodyParser from 'body-parser';
// import logger from 'morgan';
import http from 'http';
import app from '../app';

const PORT = process.env.PORT || 3001;
app.set('port', PORT);

//
// Create your proxy server and set the target in the options.
//
var proxy = httpProxy.createProxyServer({ secure: false, target: 'http://localhost:9000' });

proxy.on('proxyReq', function (proxyReq, req, res, options) {
    console.log("proxying for", req.url);
    //set headers
    console.log('proxy request forwarded succesfully');
});

proxy.on('error', function (err, req, res) {
    res.writeHead(500, {
        'Content-Type': 'text/plain'
    });
    res.end('Something went wrong. And we are reporting a custom error message.');
});

//----------------------App----------------------------------------
var server = http.createServer(app);
server.listen(PORT);
server.on('listening', onListening)

function onListening() {
    var addr = server.address();
    console.log("product listening on port:", addr.port);
}
app.use(function (req, res) {
    proxy.web(req, res)
});

//
// Create your target server
//
http.createServer(function (req, res) {
    console.log('proxy target success')
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('request successfully proxied!' + '\n' + JSON.stringify(req.headers, true, 2));
    res.end('proxy target success');
}).listen(9000);

proxy.on('proxyRes', function (proxyRes, req, res) {
    console.log('RAW Response from the target', JSON.stringify(proxyRes.headers, true, 2));
});



