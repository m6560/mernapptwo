import ProductService from "../service/productService.js";

export const getPolicy = (req, res) => {
    console.log('success');
    res.status(200).json({ id: 1, name: 'Bod' })
}
export const createPolicy = (req, res) => {
    console.log('created');
    res.status(201).json({ id: 1, name: 'Bod' })
}
export const updatePolicy = (req, res) => {
    console.log('updated');
    res.status(204).json({ id: 1, name: 'Bod' })
}
export const getProductData = async (req, res) => {
    try {
        console.log('get dataProduct');
        const data = await ProductService.getProduct();
        console.log('done', data);
        res.status(200).json(data)
    } catch (error) {
        res.status(500).json({ message: error })
    }

}