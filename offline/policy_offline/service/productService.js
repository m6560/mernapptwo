import BaseService from "./base.service.js";
import axios from "axios";

const axiosInstance = axios.create({
    baseURL: 'http://localhost:3000/product/',
    timeout: 1000,
    headers: { 'X-Custom-Header': 'foobar' }
});

class ProductService extends BaseService {
    static getProduct() {
        return axiosInstance.get('')
            .then(function (response) {
                // handle success
                console.log('Data product:', response.data);
                return response.data;
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    }
}
export default ProductService;